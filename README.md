Kung Fu Thai & Chinese Restaurant in Las Vegas, NV. Serving VEGAS since 1973. Dedicated to providing delicious Chinese and Thai Food recipes with an emphasis on taste, quality, and service for a reasonable price. Services include, casual dining, takeout and fast food delivery in Las Vegas.

Address: 3505 S Valley View Blvd, Las Vegas, NV 89103, USA

Phone: 702-247-4120
